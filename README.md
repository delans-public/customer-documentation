# REST-API
HTTP-сервисы позволяют принять данные из внешней системы (сайта, другой программы), обработать их и вернуть ответ c обратно во внешнюю систему.

<b>Документация:</b>

- [Курьерская служба (конфигурация 1C: УНФ)](https://gitlab.com/delans-public/customer-documentation/-/blob/main/api/Swagger-%D0%9A%D1%83%D1%80%D1%8C%D0%B5%D1%80%D1%81%D0%BA%D0%B0%D1%8F%20%D1%81%D0%BB%D1%83%D0%B6%D0%B1%D0%B0.json)

- [Управление торговлей (конфигурации 1C: УТ, 1C: Комплексная автоматизация, 1C:ERP)](https://gitlab.com/delans-public/customer-documentation/-/blob/main/api/Swagger-%D0%A3%D0%BF%D1%80%D0%B0%D0%B2%D0%BB%D0%B5%D0%BD%D0%B8%D0%B5%20%D1%82%D0%BE%D1%80%D0%B3%D0%BE%D0%B2%D0%BB%D0%B5%D0%B9.json)
